﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//ignore this for now
namespace Types_1
{
    //ignore this for now
    class Program
    {
        //remember that the Main is where your program always begins.
        //Its kind of like the main entrance to your home
        //for now ignore the static, void, string[] and the args words below
        static void Main(string[] args)
        {
            //declaring a variable of type (value type) int
            //and also assigning it a value
            int hello=10;
            //now that my variable is ready, i can use it in the following Debug statement.
            //note that debug output only shows up in the visual studio output window, not the actual program output
            //you will find debug output by going to View > Output at the top visual studio menu
            Debug.WriteLine("Main - The value of hello is + " + hello);
            //notice how although hello is an int type, its automatically convered to a string.
            //just one of the many cool magic stuff that c sharp does for you automatically

            //here is another way of doing the same as above

            int hello2;//i have declared it. however i am yet to assign a value it. So I cannot use it
            hello2 = 20;//now that I have assigned a value, the variable can be used

            Debug.WriteLine("Main - The value of hello is + " + hello2);

            //here is one more way of doing it.
            //remember how value types have a default value availabe through their constructors?
            int hello3 = new int(); //here i have declared but I dont have to assign a value before using it
            //the default value is assigned by the constructor
            Debug.WriteLine("Main - The value of hello is + " + hello3);

            //obviously I can change the value of hello3 if I want to
            hello3 = 30;
            Debug.WriteLine("Main - The value of hello is + " + hello3);
        }
    }
}
